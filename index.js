var express = require('express');
const http = require("http");
const cors = require("cors");
const bodyParser = require("body-parser");

var app = express();
const server = http.createServer(app);

app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(require("./routes/index"));

/*app.listen(8001, function () {
    console.log('Example app listening on port 8001!');
});*/

server.listen(8001, () => console.log(`server is runnig in PORT :8001`));