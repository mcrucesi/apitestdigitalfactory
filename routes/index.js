const express = require("express");
const app = express();

const test = require("./test");

app.use(test); // /api/categorias

module.exports = app;
