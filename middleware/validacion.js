
const isUndefined = require("is-undefined");

const verificaBody = async (req, res, next) => {
    console.log("VERIFY IN MIDDLEWARE::: ", req.body)
    if(!isUndefined(req.body)){
        let body = req.body;
        console.log("INFO::: ", JSON.stringify(body))
        if (isUndefined(body.rut) || isUndefined(body.celular) || isUndefined(body.correo) || isUndefined(body.renta)) {
            console.log(":::ERROR DATOS:::")
            res.status(401).json({
                result: "Argumento faltante (inválido)"
            });
        }
        else {
            next();
            /*res.status(200).json({
                result: "Validación correcta los datos entregaron fueron los siguientes: "+body.rut+", "+body.celular+", "+body.correo+", "+body.renta
            });*/
        }
    }
    else {
        res.status(401).json({
            result: "Argumento faltante (inválido)"
        });
    }
};

module.exports = {
    verificaBody
};
